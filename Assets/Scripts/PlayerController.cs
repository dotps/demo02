using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField] private float _speed = 50f;
    [SerializeField] private float _accelerationSpeed = 100f;
    [SerializeField] private float _rotationSpeed = 100f;
    [SerializeField] private ParticleSystem _accelerationParticle;
    
    private PlayerControl _playerControl;
    private Vector2 _direction;
    private Rigidbody _rb;
    private Vector3 _defaultPosition;
    private bool _isAcceleration;

    private void Awake()
    {
        _playerControl = new PlayerControl();
        _playerControl.Car.Acceleration.performed += context => SetAcceleration(true);
        _playerControl.Car.Acceleration.canceled += context => SetAcceleration(false);
        _playerControl.Car.ResetPosition.performed += context => ResetPosition();
    }
    
    private void Start()
    {
        _rb = GetComponent<Rigidbody>();
        _defaultPosition = transform.position;
    }

    private void OnEnable()
    {
        _playerControl.Enable();
    }

    private void OnDisable()
    {
        _playerControl.Disable();
    }
    
    private void FixedUpdate()
    {
        GetInput();
        Move();
        Acceleration();
        Rotation();
    }

    private void GetInput()
    {
        _direction = _playerControl.Car.Movement.ReadValue<Vector2>();
    }

    private void Move()
    {
        if (_direction.y == 0) return;
        
        Vector2 thrust = Time.fixedDeltaTime * _speed * _direction;
        Vector3 directionForce = new Vector3(thrust.x, 0, thrust.y);
        _rb.AddRelativeForce(directionForce, ForceMode.Impulse);
    }
    
    private void Acceleration()
    {
        if (!_isAcceleration) return;

        Vector3 directionForce = Time.fixedDeltaTime * _accelerationSpeed * Vector3.forward;
        _rb.AddRelativeForce(directionForce, ForceMode.Impulse);
    }
    
    private void Rotation()
    {
        if (_direction.x == 0) return;
        
        Quaternion rotation = Quaternion.Euler(0,Time.fixedDeltaTime * _rotationSpeed * _direction.x,0);
        _rb.MoveRotation(transform.rotation * rotation);
    }

    public void ResetPosition()
    {
        _rb.velocity = Vector3.zero;
        _rb.MovePosition(_defaultPosition);
        _rb.MoveRotation(Quaternion.identity);
    }

    public void SetAcceleration(bool isActive)
    {
        _isAcceleration = isActive;
        
        if (isActive) _accelerationParticle.Play();
        else _accelerationParticle.Stop();
    }
}
